
from Dataset.test_dataset import test_air_quality
from Model import SVM
from Utils.Metrics import r2
validation = test_air_quality()
validation.read_data()
validation.print_resume()
r2=r2()

ro_space=[30,50,70]
epsilon_space=[0.001,0.005,0.01,0.05,0.1,0.51]
Lambda_space=[0.001,0.005,0.01,0.03,0.05,0.1,0.5,1]

tot = len(ro_space)*len(epsilon_space)*len(Lambda_space)
score =''
i = 0
for ro in ro_space:
    for epsilon in epsilon_space:
        for Lambda in Lambda_space:
            print('| Iter:',i,'of:',tot,'|')
            model=SVM.ParSVR(n_jobs=3, n_splits=3, epochs=100, epsilon=epsilon, ro=ro, Lambda=Lambda)
            model.fit(validation)
            tr = r2.score(Y_hat=model.predict(validation.training_set[0]),Y_true=validation.training_set[1])
            te = r2.score(Y_hat=model.predict(validation.test_set[0]),Y_true=validation.test_set[1])
            text = 'ro:'+str(ro)+',epsilon:'+str(epsilon)+',Lambda:'+str(Lambda)+',tr_score:'+str(tr)+',te_score:'+str(te)
            score += '\n'+text
            i+=1

print(score)
with open("Crossvalidation_results.txt", "w") as text_file:
    text_file.write(score)
