import abc

from Dataset import Dataset
from Utils.Metrics import Metrics

class AbstractModel(abc.ABC):
    @abc.abstractmethod
    def __init__(self,scorer:Metrics, verbose:bool=False):
        self.scorer = scorer
        self.A = None
        self.verbose=verbose
        if self.verbose: print('*** MODEL '+self.__class__.__name__ + ' ***')

    def fit(self,dataset:Dataset):
        pass

    def predict(self,X):
        pass

    def score(self,X,Y):
        return self.scorer.print_score(Y_true=Y,Y_hat=self.predict(X))