from Dataset.Dataset import air_quality
from Dataset.reduced_dataset import reduced_dataset
from Trainer import Trainer
from Model import Lasso, Ridge, SVM
from Utils.Metrics import MSE,r2
# air_quality=air_quality(pol_expansion=2,include_bias=True)
air_quality=reduced_dataset(pol_expansion=2,include_bias=True)

# air_quality = test_air_quality()
air_quality.read_data()
air_quality.print_resume()

trainer = Trainer(dataset=air_quality)

# Add metrics
trainer.addMetric(r2())
trainer.addMetric(MSE())

# Add models
# trainer.addModel(Lasso.ParLasso(epochs=10, n_splits=8, n_jobs=1))
# trainer.addModel(Ridge.ParRidge(epochs=20,n_splits=8,n_jobs=8))
trainer.addModel(SVM.ParSVR(ro=30, epsilon=.1, Lambda=1, n_splits=20, n_jobs=8, epochs=28, verbose=True))

# Train all and compute scores
trainer.fitAll()
trainer.scoreAll(plot=True)

