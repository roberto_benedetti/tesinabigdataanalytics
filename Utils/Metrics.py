import abc
import numpy as np

class Metrics(abc.ABC):
    def score(self, Y_true:np.array,Y_hat:np.array):
        try:
            return self.compute(Y_true=Y_true,Y_hat=Y_hat)
        except:
            return Exception('ERROR: Size mismatch')

    def print_score(self, Y_true:np.array,Y_hat:np.array):
        return self.__class__.__name__ + ': ' + str(self.score(Y_true=Y_true, Y_hat=Y_hat))

    @abc.abstractmethod
    def compute(self,Y_true:np.array,Y_hat:np.array):
        pass

class MSE(Metrics):
    def compute(self,Y_true, Y_hat):
        return sum(np.power((Y_true- Y_hat),2))/len(Y_true)

class r2(Metrics):
    def compute(self,Y_true, Y_hat):
        return 1 - np.sum(np.power(Y_true-Y_hat,2))/np.sum(np.power(Y_true-np.mean(Y_true),2))