import pandas as pd
import MLlib as mlib

from Dataset.Dataset import Dataset

class reduced_dataset(Dataset):
    def __init__(self, seed = 777, path='Dataset/raw/', pol_expansion:int=1, interaction_only:bool=True,include_bias:bool=True):
        super().__init__(path=path)
        self.pol_expansion=pol_expansion
        self.seed = seed
        self.include_bias=include_bias
        self.interaction_only =interaction_only

    def read_data(self):
        '''Allocate normalized dataset on RAM'''
        xcols = ['1', '2', '3', '4', '5', '6', '7', '8',
                 '9', '10', '11', '12', '13', '14', '15', '16']
        ycols = ['CO conc (ppm)']

        df_co = pd.read_csv(self.path + 'reduced.csv')
        df_co = mlib.shuffleDataframe(df_co, seed = self.seed)

        df_tr, df_te = mlib.splitDataframe(df_co, .7)

        self.training_set = [df_tr[xcols].values, df_tr[ycols].values.flatten()]
        self.test_set = [df_te[xcols].values, df_te[ycols].values.flatten()]

        from sklearn.preprocessing import PolynomialFeatures
        pol = PolynomialFeatures(degree=self.pol_expansion, interaction_only=self.interaction_only, include_bias=self.include_bias)
        self.training_set[0]=pol.fit_transform(self.training_set[0])
        self.test_set[0] = pol.fit_transform(self.test_set[0])
        self.compute_normalization()
