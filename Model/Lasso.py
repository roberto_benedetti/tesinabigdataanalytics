from Dataset import Dataset
from Model.AbstractModel import AbstractModel
from Utils.Metrics import MSE, Metrics,r2
from joblib import Parallel, delayed

import numpy as np
from numpy.linalg import inv
import time

class ParLasso(AbstractModel):
    '''LASSO SPLITTING ACROSS SAMPLES ADMM FORMULATION:
    Stephen Boyd, Neal Parikh, Eric Chu, Borja Peleato and Jonathan Eckstein,
    "Distributed Optimization and Statistical Learning via the Alternating Direction Method of Multipliers"
            x_i_k+1 = (A_i.T*A_i + ro*Id)^-1 * (A_i.T*b_i + ro(z_k-u_i_k))
            z_k+1   = SoftTresholding_lambda/(ro*N)(mean(x_k+1)+mean(u_k))
            u_i_k+1 = u_i_k + x_i_k+1 - z_k+1

    '''

    def __init__(self, ro:float=1.0, Lambda:float=1.0, scorer:Metrics= MSE(), n_splits:int = 5, n_jobs=1, epochs=10, seed = 555):
        super().__init__(scorer= scorer)
        self.ro = ro
        self.Lambda=Lambda
        self.n_splits = n_splits
        self.n_jobs = n_jobs
        self.seed = seed
        self.epochs = epochs
        np.random.seed(self.seed)

        print('ParLasso\n\t| num_splits:', n_splits, ' \n\t| n_jobs:', n_jobs, ' \n\t| epochs:', epochs, '\n\t', '-' * 40)
    def fit(self,dataset:Dataset):
        num_feat = dataset.training_set[0].shape[1]
        num_samples = dataset.training_set[0].shape[0]

        A = dataset.training_set[0]
        b = dataset.training_set[1]
        # Free some memory
        dataset =None
        def step_1(A_i:list,B_i:list,U_i:list,z_k):
            def par1(a_i,b_i,z_k,u_k):
                x_i =  np.dot(inv(np.dot(a_i.T, a_i) +
                                 self.ro * np.identity(a_i.shape[1])) , (np.dot(a_i.T,b_i).flatten() +
                                                                           self.ro *(z_k - u_k)))
                return x_i

            return Parallel(n_jobs=self.n_jobs, verbose=0, prefer='threads')\
                (delayed(par1)(a_i=x, b_i=y, z_k=z_k, u_k=z) for x,y,z in zip(A_i,B_i,U_i))

        def softTresholding(x, treshold):
            for i in range(len(x)):
                if x[i] > treshold :
                    x[i] = x[i]-treshold
                elif x[i] < -treshold:
                    x[i] = x[i] + treshold
                else:
                    x[i]=0
                return x
        def step_2(x_list:list,u_list:list):

            x_avg = sum(x_list)/len(x_list)
            u_avg = sum(u_list)/len(u_list)

            return softTresholding(x_avg + u_avg, treshold=self.Lambda / (self.ro * len(x_list)))

        def step_3(u_k,x_1,z_1):
            u = []
            for i in range(len(u_k)):
                u_i = u_k[i] + x_1[i] - z_1
                u.append(u_i)
            return u

        # Begin Training
        t = time.time()
        # Split input across samples and initialize shared variables
        split_size = int(np.floor(len(A) / self.n_splits))
        A_i = []
        B_i = []
        X_i = []
        U_i = []
        for i in range(self.n_splits):
            A_i.append(A[i*split_size:(i+1)*split_size])
            B_i.append(b[i*split_size:(i+1)*split_size])
            X_i.append(np.zeros(num_feat))
            U_i.append(np.zeros(num_feat))
        z_k = np.zeros(num_feat)

        for j in range(self.epochs):
            X_i = step_1(A_i=A_i,B_i=B_i,U_i=U_i,z_k=z_k)
            z_k = step_2(x_list=X_i,u_list=U_i)
            U_i = step_3(u_k=U_i,x_1 = X_i, z_1=z_k)

            loss = np.mean(np.std(X_i,axis=0))
            print('\tEpoch: '+str(j)+ ' | Time' ,'%.4f' % (time.time()-t) , 's' + ' | Disagreement:','%.6f' % loss)
        print('Training Completed in','%.4f' % (time.time()-t)+ ' sec')
        self.A = np.array(z_k)

    def predict(self,X):
        return np.dot(X,self.A).flatten()