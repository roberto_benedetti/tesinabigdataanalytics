import numpy as np
def shuffleDataframe(df, seed=666):
    np.random.seed(seed)
    df = df.iloc[np.random.permutation(len(df))]
    df = df.reset_index(drop=True)
    return df

def splitDataframe(df,i):
    N = df.shape[0]
    tr_size = int(N*i)
    test_size = N - tr_size
    df_tr = df.iloc[0:tr_size]
    df_te = df.iloc[tr_size:]
    df_te.index = range(test_size)
    return (df_tr , df_te)


