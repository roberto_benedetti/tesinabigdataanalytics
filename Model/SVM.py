import numpy as np
import cvxpy as cvx
import time


from Dataset.Dataset import Dataset
from Model.AbstractModel import AbstractModel
from Utils.Metrics import MSE, Metrics,r2
from joblib import Parallel,delayed,dump


import matplotlib.pyplot as plt

class ParSVR(AbstractModel):
    def __init__(self,n_splits:int,n_jobs:int=1, verbose:bool=False,Lambda:float=0.5, ro:float=1.0, epsilon:float=1e-1, epochs:int=10,tol:float=5e-2):
        super().__init__(scorer=r2(),verbose=verbose)
        self.ro = ro
        self.Lambda = Lambda
        self.n_jobs = n_jobs
        self.epochs = epochs
        self.n_splits = n_splits
        self.epsilon = epsilon
        self.tol=tol
        self.A = None
        self.C = None

    def fit(self,dataset:Dataset):

        A = np.hstack((dataset.training_set[0], np.ones((len(dataset.training_set[0]),1))))
        Y =dataset.training_set[1].reshape(-1,1)

        num_feat = A.shape[1]
        num_samples = A.shape[0]

        # Free some memory
        dataset = None

        def step_1(A_i: list, Y_i:list, U_i: list, z_k):

            def par1(a_i,y_i, z_k, u_i):
                x_i = cvx.Variable((num_feat,1))
                prob = cvx.Problem(cvx.Minimize(cvx.sum(cvx.pos((cvx.abs((a_i*x_i) - y_i) - self.epsilon))) + (self.ro/2) * cvx.norm(x_i - z_k + u_i,2)))
                prob.solve()
                return x_i.value

            par_results =Parallel(n_jobs=self.n_jobs, verbose=0, prefer='processes') \
                (delayed(par1)(a_i=a, y_i=y, z_k=z_k, u_i=u) for a, y, u in zip(A_i, Y_i, U_i))

            return par_results

        def step_2(x_list: list, u_list: list):

            x_avg = sum(x_list) / len(x_list)
            u_avg = sum(u_list) / len(u_list)

            c = (num_feat * self.ro)/(2*self.Lambda + num_feat * self.ro)
            z_k1 = np.dot(c*self.C + (np.identity(num_feat)-self.C), x_avg + u_avg)
            return z_k1

        def step_3(u_k, x_1, z_1):
            u = []
            for i in range(len(u_k)):
                u_i = u_k[i] + x_1[i] - z_1
                u.append(u_i)
            return u

        # Begin Training
        t = time.time()
        # Split input across samples and initialize shared variables
        split_size = int(np.floor(len(A) / self.n_splits))
        if self.verbose:
            print('Train split size:',split_size)
        A_i = []
        X_i = []
        Y_i = []
        U_i = []
        self.C = np.identity(num_feat)
        self.C[num_feat-1:,num_feat-1:]=0
        for i in range(self.n_splits):
            A_i.append(A[i*split_size:(i + 1) * split_size])
            Y_i.append(Y[i*split_size:(i + 1) * split_size])

            X_i.append(np.zeros((num_feat,1)))
            U_i.append(np.zeros((num_feat,1)))
        z_k = np.zeros((num_feat,1))

        # free memory
        A = None
        Y = None
        loss = np.inf
        loss_prec = np.inf
        disagreements = []

        if self.verbose: print('Train Start')
        try:
            for j in range(self.epochs):
                X_i = step_1(A_i = A_i, Y_i = Y_i, U_i = U_i, z_k = z_k)
                z_k = step_2(x_list = X_i, u_list = U_i)
                U_i = step_3(u_k = U_i, x_1 = X_i, z_1 = z_k)

                loss = np.mean(np.std(X_i, axis = 0 ))
                disagreements.append(loss)
                if self.verbose:
                    print('\tEpoch: ' + str(j) + ' | Time', '%.4f' % (time.time() - t), 's' + ' | Disagreement:', '%.6f' % loss)
                if (np.abs(loss-loss_prec)<self.tol):
                    print('\tConvergence reached')
                    break
        except KeyboardInterrupt:
            print('-' * 89)
            print('Exiting from training early')
        print('Training Completed in', '%.4f' % (time.time() - t) + ' sec', '| Disagreement:', '%.6f' % loss)
        # dump(disagreements,'disagreements.job')
        plt.figure()
        plt.plot(disagreements, color='b',linewidth=1, marker='x',label='disagreement')
        plt.xlabel('train iteration')
        plt.ylabel('disagreement [avg variance]')
        plt.grid()
        plt.legend()
        plt.savefig('disagreement.png')
        plt.show()
        plt.close()

        # Store computed model
        self.A = np.array(z_k)

    def predict(self,X):
        X = np.hstack((X,np.ones((len(X),1))))
        return np.dot(X,self.A).flatten()