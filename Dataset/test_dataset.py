import pandas as pd
import MLlib as mlib

from Dataset.Dataset import Dataset

class test_air_quality(Dataset):
    def __init__(self, seed = 555, path='Dataset/raw/air_quality/'):
        super().__init__(path=path)
        self.seed = seed


    def read_data(self):
        '''Allocate normalized dataset on RAM'''
        xcols = ['1', '2', '3', '4', '5', '6', '7', '8',
                 '9', '10', '11', '12', '13', '14', '15', '16']
        ycols = ['CO conc (ppm)']

        df_co = pd.read_csv(self.path + '/test_df.csv')
        df_co = mlib.shuffleDataframe(df_co, seed = self.seed)
        df_co=df_co[0:600]

        df_tr, df_te = mlib.splitDataframe(df_co, .7)

        self.training_set = [df_tr[xcols].values, df_tr[ycols].values.flatten()]
        self.test_set = [df_te[xcols].values, df_te[ycols].values.flatten()]

        from sklearn.preprocessing import PolynomialFeatures
        pol = PolynomialFeatures(degree=2, interaction_only=True, include_bias=True)
        self.training_set[0]=pol.fit_transform(self.training_set[0])
        self.test_set[0] = pol.fit_transform(self.test_set[0])
        self.compute_normalization()
