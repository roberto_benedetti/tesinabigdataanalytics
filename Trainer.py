from Dataset import Dataset
from Model import AbstractModel
from Utils import Metrics
import matplotlib.pyplot as plt

class Trainer():
    def __init__(self, dataset:Dataset):
        self.listModels = []
        self.dataset = dataset
        self.listMetrics=[]

    def addModel(self, model:AbstractModel):
        self.listModels.append(model)

    def addMetric(self,metric:Metrics):
        self.listMetrics.append(metric)

    def fitAll(self):
        for model in self.listModels:
            model.fit(dataset=self.dataset)

    def scoreAll(self, plot:bool=True):
        for model in self.listModels:
            print('**MODEL ' + model.__class__.__name__ + '**')
            print(model.__class__.__name__ + ' Training scores:')
            for metric in self.listMetrics:
                print('\t', metric.print_score(Y_hat=model.predict(self.dataset.training_set[0]), Y_true=self.dataset.training_set[1]))
            if plot:
                plt.figure(figsize=(20,10))
                plt.title('MODEL: '+ str(model.__class__.__name__))
                plt.plot(self.dataset.training_set[1], color='b',label='true training label')
                plt.plot(model.predict(self.dataset.training_set[0]), color='r',label='prediction')
                plt.grid()
                plt.legend()
                plt.show()
                plt.close()
            print(model.__class__.__name__ + ' Test scores:')

            for metric in self.listMetrics:
                print('\t', metric.print_score(Y_hat=model.predict(self.dataset.test_set[0]), Y_true=self.dataset.test_set[1]))
            if plot:
                plt.figure(figsize=(20,10))
                plt.title('MODEL:'+ model.__class__.__name__)
                plt.plot(self.dataset.test_set[1], color='b',label='true test label')
                plt.plot(model.predict(self.dataset.test_set[0]), color='r',label='prediction')
                plt.grid()
                plt.legend()
                plt.show()
                plt.close()
